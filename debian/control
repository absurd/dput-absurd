Source: dput
Section: devel
Priority: optional
Maintainer: Ben Finney <bignose@debian.org>
Build-Depends-Indep:
    python3-testtools,
    python3-testscenarios,
    python3-debian,
    python3-gpg,
    python3-setuptools,
    python3-xdg,
    python3 (>= 3.4),
    bash-completion,
    dh-python
Build-Depends:
    debhelper-compat (= 13)
Standards-Version: 4.7.0
VCS-Git: https://salsa.debian.org/debian/dput.git
VCS-Browser: https://salsa.debian.org/debian/dput/
Homepage: https://packages.debian.org/source/sid/dput
Rules-Requires-Root: no

Package: dput
Architecture: all
Depends:
    python3-pkg-resources,
    ${python3:Depends},
    ${misc:Depends}
Suggests: openssh-client, lintian, mini-dinstall, rsync
Description: Debian package upload tool
 DPut is the Debian Package Upload Tool. The ‘dput’ command uploads one or more
 packages to the Debian archive.
 .
 This package also includes the ‘dcut’ command, which can generate and / or
 upload a commands file for the Debian FTP archive upload queue.
 .
 ‘dput’ runs some tests to verify that the package is compliant with Debian
 Policy. It can also run Lintian on the package before upload, and/or run
 ‘dinstall’ in dry-run-mode, when using an appropriate upload method.
 .
 DPut is intended mainly for Debian maintainers, although it can also be useful
 for people maintaining local APT repositories.
